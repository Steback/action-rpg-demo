#ifndef PROTOTYPE_ACTION_RPG_IMGUIBINDINGS_HPP
#define PROTOTYPE_ACTION_RPG_IMGUIBINDINGS_HPP


namespace sol {
    class state;
}

namespace engine::lua {

    void setImGuiBindings(sol::state& label);

}


#endif //PROTOTYPE_ACTION_RPG_IMGUIBINDINGS_HPP
